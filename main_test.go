package main

import (
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
	"tic-tac-toe-golang/tic_tac_toe"
)

func Test_randomPlayer(t *testing.T) {
	playerO := 0
	playerX := 0

	for i := 0; i < 100000; i++ {
		if newRandomPlayer() == tic_tac_toe.PlayerO {
			playerO++
		} else {
			playerX++
		}
	}
	assert.Equal(t, 50, int(math.Round(float64(playerO) / 1000)))

}