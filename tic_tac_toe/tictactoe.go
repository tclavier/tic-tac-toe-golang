package tic_tac_toe

import (
	"fmt"
	"strings"
)

type TicTacToeGame struct {
	board [9]Cell
}

type Cell string

func (c Cell) Next() Cell {
	if c == PlayerO {
		return PlayerX
	}
	return PlayerO
}

const PlayerX Cell = "X"
const NoPlayer Cell = "No Player"
const EndOfGame Cell = "End Of Game"
const PlayerO Cell = "O"

func (g TicTacToeGame) Winner() Cell {
	if g.checkAllLines() != NoPlayer {
		return g.checkAllLines()
	}
	if g.checkAllColumns() != NoPlayer {
		return g.checkAllColumns()
	}
	if g.checkDiagonals() != NoPlayer {
		return g.checkDiagonals()
	}
	if g.countEmptyCells() > 0 {
		return NoPlayer
	}
	return EndOfGame
}

func (g TicTacToeGame) checkDiagonals() Cell {
	if g.getCell(1) == g.getCell(5) && g.getCell(5) == g.getCell(9) {
		return g.getCell(5)
	}

	if g.getCell(3) == g.getCell(5) && g.getCell(5) == g.getCell(7) {
		return g.getCell(5)
	}
	return NoPlayer
}

func (g TicTacToeGame) checkAllColumns() Cell {
	for column := 1; column <= 3; column++ {
		if g.checkColumn(column) {
			return g.getColumnWinner(column)
		}
	}
	return NoPlayer
}

func (g TicTacToeGame) checkAllLines() Cell {
	for line := 1; line <= 3; line++ {
		if g.checkLine(line) {
			return g.getLineWinner(line)
		}
	}
	return NoPlayer
}

func (g TicTacToeGame) countEmptyCells() int {
	emptyCells := 0
	for i := 1; i < 9; i++ {
		if g.getCell(i) == NoPlayer {
			emptyCells++
		}
	}
	return emptyCells
}

func (g TicTacToeGame) getLineWinner(line int) Cell {
	return g.getCell((line-1)*3 + 1)
}

func (g TicTacToeGame) checkLine(lineNumber int) bool {
	firstCell := (lineNumber-1)*3 + 1
	return g.getCell(firstCell) == g.getCell(firstCell+1) && g.getCell(firstCell+1) == g.getCell(firstCell+2)
}

func (g TicTacToeGame) getCell(i int) Cell {
	return g.board[i-1]
}

func (g TicTacToeGame) checkColumn(column int) bool {
	firstCell := column
	return g.getCell(firstCell) == g.getCell(firstCell+3) && g.getCell(firstCell+3) == g.getCell(firstCell+6)
}

func (g *TicTacToeGame) Play(player Cell, cellId int) Cell {
	if g.getCell(cellId) == NoPlayer {
		g.board[cellId-1] = player
		return player.Next()
	}
	return player
}

func (g TicTacToeGame) getColumnWinner(column int) Cell {
	return g.getCell(column)
}

func (g TicTacToeGame) Render() string {
	return fmt.Sprintf("" +
		"+---+---+---+\n"+
		"| %s | %s | %s |\n" +
		"+---+---+---+\n"+
		"| %s | %s | %s |\n" +
		"+---+---+---+\n"+
		"| %s | %s | %s |\n" +
		"+---+---+---+\n", g.renderCell(1), g.renderCell(2), g.renderCell(3), g.renderCell(4), g.renderCell(5), g.renderCell(6), g.renderCell(7), g.renderCell(8), g.renderCell(9))
}

func (g TicTacToeGame) renderCell(i int) string {
	switch g.getCell(i) {
	case PlayerO:
		return "O"
	case PlayerX:
		return "X"
	default:
		return fmt.Sprintf("%d", i)
	}
}

func NewGame(state string) TicTacToeGame {
	var board [9]Cell
	for i, cell := range strings.Replace(state, " ", "", -1) {
		board[i] = mapPlayerFromString(string(cell))
	}
	return TicTacToeGame{board}

}

func EmptyNewGame() TicTacToeGame {
	var board [9]Cell
	for i := 0; i < 9; i++ {
		board[i] = NoPlayer
	}
	return TicTacToeGame{board}
}

func mapPlayerFromString(input string) Cell {
	switch input {
	case "X":
		return PlayerX
	case "O":
		return PlayerO
	default:
		return NoPlayer
	}
}
