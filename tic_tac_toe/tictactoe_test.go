package tic_tac_toe

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWinnerRules(t *testing.T) {
	t.Run("First line", func(t *testing.T) {
		state := "" +
			"X X X" +
			"O O ." +
			"O . ."
		game := NewGame(state)
		assert.Equal(t, PlayerX, game.Winner())
	})
	t.Run("Second line", func(t *testing.T) {
		state := "" +
			"X X O" +
			"O O O" +
			"X . ."
		game := NewGame(state)
		assert.Equal(t, PlayerO, game.Winner())
	})
	t.Run("Last line", func(t *testing.T) {
		state := "" +
			"X O X" +
			"O O ." +
			"X X X"
		game := NewGame(state)
		assert.Equal(t, PlayerX, game.Winner())
	})
	t.Run("First column", func(t *testing.T) {
		state := "" +
			"X O X" +
			"X O O" +
			"X . ."
		game := NewGame(state)
		assert.Equal(t, PlayerX, game.Winner())
	})
	t.Run("Second column", func(t *testing.T) {
		state := "" +
			"X O X" +
			"X O O" +
			". O ."
		game := NewGame(state)
		assert.Equal(t, PlayerO, game.Winner())
	})
	t.Run("Third column", func(t *testing.T) {
		state := "" +
			"X O X" +
			". O X" +
			". . X"
		game := NewGame(state)
		assert.Equal(t, PlayerX, game.Winner())
	})

	t.Run("First diagonal", func(t *testing.T) {
		state := "" +
			"X O O" +
			". X O" +
			". . X"
		game := NewGame(state)
		assert.Equal(t, PlayerX, game.Winner())
	})

	t.Run("Second diagonal", func(t *testing.T) {
		state := "" +
			"X O O" +
			". O X" +
			"O . X"
		game := NewGame(state)
		assert.Equal(t, PlayerO, game.Winner())
	})

	t.Run("No Winner", func(t *testing.T) {
		game := sampleGame()
		assert.Equal(t, NoPlayer, game.Winner())
	})

	t.Run("End of game without winner", func(t *testing.T) {
		state := "" +
			"X O X" +
			"O O X" +
			"X X O"
		game := NewGame(state)
		assert.Equal(t, EndOfGame, game.Winner())
	})
}

func Test_NewGame(t *testing.T) {
	t.Run("Should init from string and get player X", func(t *testing.T) {
		game := sampleGame()
		assert.Equal(t, PlayerX, game.getCell(1))
	})
	t.Run("Should init from string and get player O", func(t *testing.T) {
		game := sampleGame()
		assert.Equal(t, PlayerO, game.getCell(2))
	})
	t.Run("Should init empty new game", func(t *testing.T) {
		game := EmptyNewGame()
		assert.Equal(t, NoPlayer, game.getCell(1))
		assert.Equal(t, NoPlayer, game.getCell(3))
		assert.Equal(t, NoPlayer, game.getCell(9))
	})
}

func Test_Play(t *testing.T) {
	t.Run("Should play one cell", func(t *testing.T) {
		game := sampleGame()
		game.Play(PlayerX, 9)
		assert.Equal(t, PlayerX, game.getCell(9))
	})

	t.Run("Should not play on non empty cell", func(t *testing.T) {
		game := sampleGame()
		newPlayer := game.Play(PlayerX, 2)
		assert.Equal(t, PlayerO, game.getCell(2))
		assert.Equal(t, PlayerX, newPlayer)
	})
}

func Test_NextPlayer(t *testing.T) {
	t.Run("Should invert player O", func(t *testing.T) {
		player := PlayerO
		assert.Equal(t, PlayerX, player.Next())
	})
	t.Run("Should invert player X", func(t *testing.T) {
		player := PlayerX
		assert.Equal(t, PlayerO, player.Next())
	})
}

func TestTicTacToeGame_Render(t *testing.T) {
	game := sampleGame()
	expected := "" +
		"+---+---+---+\n" +
		"| X | O | X |\n" +
		"+---+---+---+\n" +
		"| 4 | O | X |\n" +
		"+---+---+---+\n" +
		"| O | 8 | 9 |\n" +
		"+---+---+---+\n"
		assert.Equal(t, expected, game.Render())
}

func sampleGame() TicTacToeGame {
	state := "" +
		"X O X" +
		". O X" +
		"O . ."
	game := NewGame(state)
	return game
}
