package main

import (
	"fmt"
	"math/rand"
	"tic-tac-toe-golang/tic_tac_toe"
)

func main() {
	game := tic_tac_toe.NewGame("" +
		". . ." +
		". . ." +
		". . .")
	player := newRandomPlayer()

mainLoop:
	for {
		switch game.Winner() {
		case tic_tac_toe.EndOfGame:
			fmt.Printf("No winner!\n")
			break mainLoop
		case tic_tac_toe.NoPlayer:
			var cell int
			fmt.Println("\n" + game.Render())
			fmt.Printf("Player %s play\n", player)
			fmt.Scanf("%d", &cell)
			player =  game.Play(player, cell)
		default:
			fmt.Printf("Player %s win!\n", game.Winner())
			break mainLoop
		}
	}
}

func newRandomPlayer() tic_tac_toe.Cell {
	if rand.Intn(2) == 1 {
		return tic_tac_toe.PlayerO
	}
	return tic_tac_toe.PlayerX
}
